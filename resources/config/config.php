<?php

return [
    'related' => [
        'type'     => 'anomaly.field_type.select',
        'required' => true,
        'config'   => [
            'handler' => \Newebtime\RepeateringFieldType\Support\Config\RelatedHandler::class,
        ],
    ],
    'mapped'  => [
        'type'     => 'anomaly.field_type.text',
        'required' => true,
    ],
    'add_row' => [
        'type' => 'anomaly.field_type.text',
    ],
    'min'     => [
        'type'   => 'anomaly.field_type.integer',
        'config' => [
            'min' => 1,
        ],
    ],
    'max'     => [
        'type'   => 'anomaly.field_type.integer',
        'config' => [
            'min' => 1,
        ],
    ],
];
