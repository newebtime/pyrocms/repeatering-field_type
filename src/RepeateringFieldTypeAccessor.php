<?php namespace Newebtime\RepeateringFieldType;

use Anomaly\Streams\Platform\Addon\FieldType\FieldTypeAccessor;
use Anomaly\Streams\Platform\Entry\Contract\EntryInterface;
use Anomaly\Streams\Platform\Model\EloquentModel;
use Anomaly\Streams\Platform\Ui\Form\Multiple\MultipleFormBuilder;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class RepeateringFieldTypeAccessor
 *
 * @link   http://pyrocms.com/
 * @author PyroCMS, Inc. <support@pyrocms.com>
 * @author Ryan Thompson <ryan@pyrocms.com>
 */
class RepeateringFieldTypeAccessor extends FieldTypeAccessor
{

    /**
     * The field type object.
     * This is for IDE support.
     *
     * @var RepeateringFieldType
     */
    protected $fieldType;

    /**
     * Set the value.
     *
     * @param $value
     */
    public function set($value)
    {
        if ($value instanceof MultipleFormBuilder) {
            return;
        }

        if (is_string($value)) {
            $value = explode(',', $value);
        } elseif ($value instanceof Collection) {
            $value = $value->filter()->all();
        } elseif ($value instanceof EntryInterface) {
            $value = [$value->getId()];
        }

        if (!$value) {
            $this->fieldType->getRelation()->each(function (EloquentModel $item) {
                $item->delete();
            });

            return;
        }

        $this->fieldType->getRelation()->whereNotIn('id', $value)->each(function (EloquentModel $item) {
            $item->delete();
        });
    }

    /**
     * Get the value.
     *
     * @return mixed
     */
    public function get()
    {
        /* @var EloquentModel $entry */
        $entry = $this->fieldType->getEntry();

        $relation = camel_case($this->fieldType->getFieldName());

        /**
         * If the relation is already
         * loaded then don't load again!
         */
        if ($entry->relationLoaded($relation)) {
            return $entry->getRelation($relation);
        }

        return $this->fieldType->getRelation();
    }
}
