<?php namespace Newebtime\RepeateringFieldType\Validation;

use Anomaly\Streams\Platform\Ui\Form\Multiple\MultipleFormBuilder;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Newebtime\RepeateringFieldType\Command\GetMultiformFromPost;
use Newebtime\RepeateringFieldType\RepeateringFieldType;

/**
 * Class ValidateRepeater
 *
 * @link   http://pyrocms.com/
 * @author PyroCMS, Inc. <support@pyrocms.com>
 * @author Ryan Thompson <ryan@pyrocms.com>
 */
class ValidateRepeater
{

    use DispatchesJobs;

    /**
     * Handle the command.
     *
     * @param RepeateringFieldType $fieldType
     * @return bool
     */
    public function handle(RepeateringFieldType $fieldType)
    {
        /* @var MultipleFormBuilder $forms */
        if (!$forms = $this->dispatch(new GetMultiformFromPost($fieldType))) {
            return true;
        }

        $forms
            ->build()
            ->validate();

        if (!$forms->getFormErrors()->isEmpty()) {
            return false;
        }

        return true;
    }
}
