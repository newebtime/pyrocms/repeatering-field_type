<?php namespace Newebtime\RepeateringFieldType;

use Anomaly\Streams\Platform\Addon\AddonServiceProvider;
use Anomaly\Streams\Platform\Entry\Contract\EntryInterface;
use Anomaly\Streams\Platform\Entry\EntryModel;
use Anomaly\Streams\Platform\Ui\Form\FormBuilder;
use Illuminate\Contracts\Container\Container;

/**
 * Class RepeateringFieldTypeServiceProvider
 *
 * @link          http://pyrocms.com/
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Ryan Thompson <ryan@pyrocms.com>
 * @package       Anomaly\RepeaterFieldType
 */
class RepeateringFieldTypeServiceProvider extends AddonServiceProvider
{
    /**
     * The addon routes.
     *
     * @var array
     */
    protected $routes = [
        'repeatering-field_type/form/{field}' => 'Newebtime\RepeateringFieldType\Http\Controller\RepeateringController@form',
    ];

    /**
     * Register the addon.
     *
     * @param EntryModel $model
     */
    public function register(
        EntryModel $model
    ) {
        $model->bind(
            'new_repeatering_field_type_form_builder',
            function (Container $container) {

                /* @var EntryInterface $this */
                $builder = $this->getBoundModelNamespace() . '\\Support\\RepeateringFieldType\\FormBuilder';

                if (class_exists($builder)) {
                    return $container->make($builder);
                }

                return $container->make(FormBuilder::class);
            }
        );
    }
}
