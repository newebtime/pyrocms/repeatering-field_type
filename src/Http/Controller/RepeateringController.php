<?php namespace Newebtime\RepeateringFieldType\Http\Controller;

use Anomaly\Streams\Platform\Assignment\Contract\AssignmentInterface;
use Anomaly\Streams\Platform\Assignment\Contract\AssignmentRepositoryInterface;
use Anomaly\Streams\Platform\Http\Controller\PublicController;
use Newebtime\RepeateringFieldType\RepeateringFieldType;

/**
 * Class RepeaterController
 *
 * @link   http://pyrocms.com/
 * @author PyroCMS, Inc. <support@pyrocms.com>
 * @author Ryan Thompson <ryan@pyrocms.com>
 */
class RepeateringController extends PublicController
{
    /**
     * Return a form row.
     *
     * @param AssignmentRepositoryInterface $assignments
     * @param                               $assignment
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function form(AssignmentRepositoryInterface $assignments, $assignment)
    {
        /* @var AssignmentInterface $assignment */
        $assignment = $assignments->find($assignment);

        /* @var RepeateringFieldType $type */
        $type = $assignment->getFieldType();

        $type->setPrefix($this->request->get('prefix'));

        return $type
            ->form($assignment, $this->request->get('instance'))
            ->addFormData('field_type', $type)
            ->setSkips([$type->config('mapped')])
            ->render();
    }
}
