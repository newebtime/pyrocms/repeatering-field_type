<?php namespace Newebtime\RepeateringFieldType\Command;

use Anomaly\Streams\Platform\Assignment\Contract\AssignmentInterface;
use Anomaly\Streams\Platform\Assignment\Contract\AssignmentRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryCollection;
use Anomaly\Streams\Platform\Ui\Form\Multiple\MultipleFormBuilder;
use Newebtime\RepeateringFieldType\RepeateringFieldType;

/**
 * Class GetMultiformFromData
 *
 * @link   http://pyrocms.com/
 * @author PyroCMS, Inc. <support@pyrocms.com>
 * @author Ryan Thompson <ryan@pyrocms.com>
 */
class GetMultiformFromData
{

    /**
     * The field type instance.
     *
     * @var RepeateringFieldType
     */
    protected $fieldType;

    /**
     * Create a new GetMultiformFromData instance.
     *
     * @param RepeateringFieldType $fieldType
     */
    public function __construct(RepeateringFieldType $fieldType)
    {
        $this->fieldType = $fieldType;
    }

    /**
     * Get the multiple form builder from the value.
     *
     * @param AssignmentRepositoryInterface $assignments
     * @param MultipleFormBuilder           $forms
     *
     * @return MultipleFormBuilder|null
     */
    public function handle(AssignmentRepositoryInterface $assignments, MultipleFormBuilder $forms)
    {
        /* @var EntryCollection $value */
        if (!$value = $this->fieldType->getValue()) {
            return null;
        }

        foreach ($value as $item) {
            /* @var AssignmentInterface $assignment */
            if (!$assignment = $assignments->find($item['field'])) {
                continue;
            }

            /* @var RepeateringFieldType $type */
            $type = $assignment->getFieldType();

            $type->setPrefix($this->fieldType->getPrefix());

            $form = $type
                ->form($assignment, $item['instance']);

            if ($item['entry']) {
                $form->setEntry($item['entry']);
            }

            $form->setSkips([$this->fieldType->config('mapped')]);
            
            $form->setReadOnly($this->fieldType->isReadOnly());

            $forms->addForm($this->fieldType->getFieldName() . '_' . $item['instance'], $form);
        }

        $forms->setOption('success_message', false);

        return $forms;
    }
}
