<?php namespace Newebtime\RepeateringFieldType\Command;

use Anomaly\Streams\Platform\Assignment\Contract\AssignmentInterface;
use Anomaly\Streams\Platform\Assignment\Contract\AssignmentRepositoryInterface;
use Anomaly\Streams\Platform\Ui\Form\Multiple\MultipleFormBuilder;
use Illuminate\Http\Request;
use Newebtime\RepeateringFieldType\RepeateringFieldType;

/**
 * Class GetMultiformFromPost
 *
 * @link   http://pyrocms.com/
 * @author PyroCMS, Inc. <support@pyrocms.com>
 * @author Ryan Thompson <ryan@pyrocms.com>
 */
class GetMultiformFromPost
{

    /**
     * The field type instance.
     *
     * @var RepeateringFieldType
     */
    protected $fieldType;

    /**
     * Create a new GetMultiformFromPost instance.
     *
     * @param RepeateringFieldType $fieldType
     */
    public function __construct(RepeateringFieldType $fieldType)
    {
        $this->fieldType = $fieldType;
    }

    /**
     * Handle the command.
     *
     * @param AssignmentRepositoryInterface $assignments
     * @param MultipleFormBuilder           $forms
     * @param Request                       $request
     *
     * @return MultipleFormBuilder|null
     */
    public function handle(AssignmentRepositoryInterface $assignments, MultipleFormBuilder $forms, Request $request)
    {
        if (!$request->has($this->fieldType->getFieldName())) {
            return null;
        }

        foreach ($request->get($this->fieldType->getFieldName()) as $item) {
            /* @var AssignmentInterface $assignment */
            if (!$assignment = $assignments->find($item['field'])) {
                continue;
            }

            /* @var RepeateringFieldType $type */
            $type = $assignment->getFieldType();

            $type->setPrefix($this->fieldType->getPrefix());

            $form = $type->form($assignment, $item['instance']);

            if ($item['entry']) {
                $form->setEntry($item['entry']);
            }

            try {
                $form->build();
                $form->setSkips([$this->fieldType->config('mapped')]);
                $form->getForm()->disableField($this->fieldType->config('mapped'));
            } catch(\Exception $e) {
                dd($item);
            }

            $form->setReadOnly($this->fieldType->isReadOnly());

            $forms->addForm($this->fieldType->getFieldName() . '_' . $item['instance'], $form);
        }

        $forms->setOption('success_message', false);

        return $forms;
    }
}
