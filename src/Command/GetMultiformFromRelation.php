<?php namespace Newebtime\RepeateringFieldType\Command;

use Anomaly\Streams\Platform\Assignment\Contract\AssignmentInterface;
use Anomaly\Streams\Platform\Assignment\Contract\AssignmentRepositoryInterface;
use Anomaly\Streams\Platform\Entry\Contract\EntryInterface;
use Anomaly\Streams\Platform\Entry\EntryCollection;
use Anomaly\Streams\Platform\Field\Contract\FieldInterface;
use Anomaly\Streams\Platform\Field\Contract\FieldRepositoryInterface;
use Anomaly\Streams\Platform\Ui\Form\Multiple\MultipleFormBuilder;
use Newebtime\RepeateringFieldType\RepeateringFieldType;

/**
 * Class GetMultiformFromRelation
 *
 * @link   http://pyrocms.com/
 * @author PyroCMS, Inc. <support@pyrocms.com>
 * @author Ryan Thompson <ryan@pyrocms.com>
 */
class GetMultiformFromRelation
{

    /**
     * The field type instance.
     *
     * @var RepeateringFieldType
     */
    protected $fieldType;

    /**
     * Create a new GetMultiformFromRelation instance.
     *
     * @param RepeateringFieldType $fieldType
     */
    public function __construct(RepeateringFieldType $fieldType)
    {
        $this->fieldType = $fieldType;
    }

    /**
     * Get the multiple form builder from the value.
     *
     * @param AssignmentRepositoryInterface $assignments
     * @param MultipleFormBuilder           $forms
     *
     * @return MultipleFormBuilder|null
     */
    public function handle(AssignmentRepositoryInterface $assignments, MultipleFormBuilder $forms)
    {
        /* @var EntryCollection $value */
        if (!$value = $this->fieldType->getValue()) {
            return null;
        }

        /* @var EntryInterface $entry */
        foreach ($value as $instance => $entry) {
            /* @var AssignmentInterface $assignment */
            if (!$assignment = $assignments->find($this->fieldType->id())) {
                continue;
            }

            /* @var RepeateringFieldType $type */
            $type = $assignment->getFieldType();

            $type->setPrefix($this->fieldType->getPrefix());

            $form = $type
                ->form($assignment, $instance)
                ->setEntry($entry);


            $form->setSkips([$this->fieldType->config('mapped')]);
            
            $form->setReadOnly($this->fieldType->isReadOnly());

            $forms->addForm($this->fieldType->getFieldName() . '_' . $instance, $form);
        }

        $forms->setOption('success_message', false);

        return $forms;
    }
}
